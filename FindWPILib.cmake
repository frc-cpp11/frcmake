# CMake Find Module for WPILib by Matt Coffin

# Common attributes
set(WPILIB_SUFFIXES wpilib WPILib)
set(WPILIB_NAMES WPILib wpilib)

# Find includes
find_path(WPILib_INCLUDE_DIRS
	NAMES WPILib.h
	PATHS /usr/powerpc-wrs-vxworks/include /usr/include /usr/local/powerpc-wrs-vxworks/include
	PATH_SUFFIXES ${WPILIB_SUFFIXES}
)

# Library
find_library(WPILib_LIBRARY
	NAMES ${WPILIB_NAMES}
	PATHS /usr/powerpc-wrs-vxworks/lib /usr/lib /usr/local/lib /usr/local/powerpc-wrs-vxworks/lib
	PATH_SUFFIXES ${WPILIB_SUFFIXES}
)

# Handle Args
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(WPILib DEFAULT_MSG WPILib_LIBRARY WPILib_INCLUDE_DIRS)

if (WPILib_FOUND)
	set(WPILib_LIBRARIES ${WPILib_LIBRARY})
else (WPILib_FOUND)
	set( WPILib_LIBRARIES )
endif (WPILib_FOUND)

mark_as_advanced(
	WPILib_LIBRARY
	WPILib_INCLUDE_DIRS
)
